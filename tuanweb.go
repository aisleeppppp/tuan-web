package tuanWeb

import (
	"html/template"
	"log"
	"net/http"
	"path"
	"strings"
)

//HandlerFunc 是定义路由映射处理方法 的方法类型
type HandlerFunc func(*Context)

//Engine 中的router是一个map 是 路由 与 处理方法 的映射关系
//key是由请求方法+路径组成 如： GET-/hello/api/123
type Engine struct {
	*RouterGroup //嵌入（继承） 指向RouterGroup，可有可无
	router       *router
	groups       []*RouterGroup //存储所有的RouterGroup
	//下面两个用作html模板渲染
	htmlTemplates *template.Template //将所有的模板加载到内存
	funcMap       template.FuncMap   //自定义的模板渲染函数
}

//RouterGroup 路由分组控制的抽象
//engine字段指向Engine Engine中也有一个指向RouterGroup的指针，且还有一个[]*RouterGroup
//这样 Engine和RouterGroup 直接就能互相访问到了
type RouterGroup struct {
	prefix      string //前缀(prefix)，比如/，或者/api
	middlewares []HandlerFunc
	parent      *RouterGroup //要支持分组嵌套，那么需要知道当前分组的父亲(parent)是谁；
	engine      *Engine      //所有的分组都用一个Engine实例
}

func New() *Engine {
	engine := &Engine{router: newRouter()}
	engine.RouterGroup = &RouterGroup{engine: engine}
	engine.groups = []*RouterGroup{engine.RouterGroup}
	return engine
}

func Default() *Engine {
	engine := New()
	engine.Use(Logger(), Recovery())
	return engine
}

func (group *RouterGroup) Group(prefix string) *RouterGroup {
	engine := group.engine
	newGroup := &RouterGroup{
		prefix: group.prefix + prefix,
		parent: group,
		engine: engine,
	}
	engine.groups = append(engine.groups, newGroup)
	return newGroup
}

func (group *RouterGroup) addRoute(method string, comp string, handler HandlerFunc) {
	pattern := group.prefix + comp
	log.Printf("Route %4s - %s", method, pattern)
	group.engine.router.addRoute(method, pattern, handler)
}

//GET & POST 都是调用 addRoute 将key 和 处理方法添加到router 中。
//不同之处在于： GET 加的key 是GET-     POST加的是POST-
//之所以这样写也只是为了方便调这些方法
func (group *RouterGroup) GET(pattern string, handler HandlerFunc) {
	group.addRoute("GET", pattern, handler)
}

func (group RouterGroup) POST(pattern string, handler HandlerFunc) {
	group.addRoute("POST", pattern, handler)
}

//Run 封装了ListenAndServe 此方法中包含了大量的建立连接，等待连接，相应连接的逻辑，帮助我们简化了代码
//想要学习更多的知识，可以了解ListenAndServe的代码
func (engine *Engine) Run(addr string) (err error) {
	return http.ListenAndServe(addr, engine)
}

//Use 将中间件应用到某个Group
func (group *RouterGroup) Use(middlewares ...HandlerFunc) {
	group.middlewares = append(group.middlewares, middlewares...)
}

//tips:Engine要实现ServeHTTP才能实现Handler接口，才能传参到 http.ListenAndServe
func (engine *Engine) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	var middlewares []HandlerFunc
	for _, group := range engine.groups {
		if strings.HasPrefix(req.URL.Path, group.prefix) {
			middlewares = append(middlewares, group.middlewares...)
		}
	}
	c := NewContext(w, req)
	c.handlers = middlewares
	c.engine = engine
	engine.router.handle(c)
}

func (group *RouterGroup) createStaticHandler(relativePath string, fs http.FileSystem) HandlerFunc {
	absolutePath := path.Join(group.prefix, relativePath)
	fileServer := http.StripPrefix(absolutePath, http.FileServer(fs))
	return func(c *Context) {
		file := c.Param("filepath")
		if _, err := fs.Open(file); err != nil {
			c.Status(http.StatusNotFound)
			return
		}
		fileServer.ServeHTTP(c.Writer, c.Req)
	}
}

func (group *RouterGroup) Static(relativePath string, root string) {
	handler := group.createStaticHandler(relativePath, http.Dir(root))
	urlPattern := path.Join(relativePath, "/*filepath")
	group.GET(urlPattern, handler)
}

//SetFuncMap 设置自定义渲染函数
func (engine *Engine) SetFuncMap(funcMap template.FuncMap) {
	engine.funcMap = funcMap
}

//LoadHTMLGlob 加载模板
func (engine *Engine) LoadHTMLGlob(pattern string) {
	engine.htmlTemplates = template.Must(template.New("").Funcs(engine.funcMap).ParseGlob(pattern))
}
