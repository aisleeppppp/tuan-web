package tuanWeb

import "strings"

//暂定实现的动态路由有两种：
//参数匹配: 如： /p/:lang/doc 可以匹配/p/c/doc 得到参数（lang:c）
//通配* 如： /static/*filepath 可以匹配/static/hhh.jpg 得到(filepath:hhh.jpg)

//Trie树结构的 每一个节点 的抽象
type node struct {
	pattern  string  //待匹配的路由 完整的待匹配路由
	part     string  //路由中的一部分
	children []*node //指向子节点
	isWild   bool    //是否模糊匹配
}

func (n *node) matchChild(part string) *node {
	for _, child := range n.children {
		if child.part == part || child.isWild {
			return child
		}
	}
	return nil
}

func (n *node) matchChildren(part string) []*node {
	nodes := make([]*node, 0)
	for _, child := range n.children {
		if child.part == part || child.isWild {
			nodes = append(nodes, child)
		}
	}
	return nodes
}

//insert 插入结点
//递归查询每一层节点，如果没有匹配到当前part的节点，则新建一个
//注意：举例：/p/:lang/doc只有在第三层节点即doc节点。将doc节点的pattern设置为/p/:lang/doc
//p和:lang节点的pattern都为空。
//因此，当我们匹配结束时，我们可以使用n.pattern == ""来判断路由规则是否匹配成功。
func (n *node) insert(pattern string, parts []string, height int) {
	if len(parts) == height {
		n.pattern = pattern
		return
	}

	part := parts[height]
	child := n.matchChild(part)
	if child == nil {
		child = &node{part: part, isWild: part[0] == ':' || part[0] == '*'}
		n.children = append(n.children, child)
	}
	child.insert(pattern, parts, height+1)
}

//search 搜索结点
//同样也是递归查询每一层的节点，退出规则是，匹配到了*，匹配失败，或者匹配到了第len(parts)层节点。
func (n *node) search(parts []string, height int) *node {
	if len(parts) == height || strings.HasPrefix(n.part, "*") {
		if n.pattern == "" {
			return nil
		}
		return n
	}

	part := parts[height]
	children := n.matchChildren(part)

	for _, child := range children {
		result := child.search(parts, height+1)
		if result != nil {
			return result
		}
	}
	return nil
}
