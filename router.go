package tuanWeb

import (
	"net/http"
	"strings"
)

type router struct {
	//roots 例如：roots['GET'], roots['POST']
	//每一种不同的请求方式，会有各自的一个Trie树
	//handlers 例如：handlers['GET-/p/:name/ok']
	//是请求与处理函数的映射
	roots    map[string]*node       //存储的是 每种请求方式 对应的 Trie树根节点 的映射
	handlers map[string]HandlerFunc //存储 每个请求 与 处理函数 的映射
}

func newRouter() *router {
	return &router{
		handlers: make(map[string]HandlerFunc),
		roots:    make(map[string]*node),
	}
}

func parsePattern(pattern string) []string {
	vs := strings.Split(pattern, "/")

	parts := make([]string, 0)
	for _, item := range vs {
		if item != "" {
			parts = append(parts, item)
			if item[0] == '*' {
				break
			}
		}
	}
	return parts
}

func (r *router) addRoute(method string, pattern string, handler HandlerFunc) {
	parts := parsePattern(pattern)

	key := method + "-" + pattern
	_, ok := r.roots[method]
	if !ok {
		r.roots[method] = &node{}
	}
	r.roots[method].insert(pattern, parts, 0)
	r.handlers[key] = handler
}

func (r *router) handle(c *Context) {
	n, params := r.getRoute(c.Method, c.Path)
	if n != nil {
		c.Params = params
		key := c.Method + "-" + n.pattern
		c.handlers = append(c.handlers, r.handlers[key])
	} else {
		c.handlers = append(c.handlers, func(c *Context) {
			c.String(http.StatusNotFound, "404 not found: %s\n", c.Path)
		})
	}
	c.Next()
}

//getRoute 解析 ： * 这两种匹配符的参数，返回一个map
//e.g. /p/go/doc匹配到/p/:lang/doc，解析结果为：{lang: "go"}
//     /static/css/hhh.css匹配到/static/*filepath，解析结果为：{filepath: "css/hhh.css"}
func (r *router) getRoute(method string, path string) (*node, map[string]string) {
	searchParts := parsePattern(path)
	params := make(map[string]string)
	root, ok := r.roots[method]

	if !ok {
		return nil, nil
	}

	n := root.search(searchParts, 0)

	if n != nil {
		parts := parsePattern(n.pattern)
		for index, part := range parts {
			if part[0] == ':' {
				params[part[1:]] = searchParts[index]
			}
			if part[0] == '*' && len(part) > 1 {
				params[part[1:]] = strings.Join(searchParts[index:], "/")
				break
			}
		}
		return n, params
	}
	return nil, nil
}
