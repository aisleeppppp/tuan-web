package tuanWeb

import (
	"encoding/json"
	"fmt"
	"net/http"
)

type H map[string]interface{}

//Context 是对 Request 和 Response
//中 原本就有的数据的简化使用(因为http提供的ResponseWriter和Request粒度太细，导致使用的时候重复和易错)
//以及对其他需求比如动态路由，中间件, 输出特定格式信息 等 的支持
//的抽象，
type Context struct {
	Writer     http.ResponseWriter
	Req        *http.Request
	Path       string
	Method     string
	Params     map[string]string //使Context能够处理 解析请求 而得到的参数，解析到的路由参数会存储到Params中
	StatusCode int

	//handlers & index 是为了支持中间件
	handlers []HandlerFunc
	index    int

	//便可以访问Engine中的html模板
	engine *Engine
}

func NewContext(w http.ResponseWriter, req *http.Request) *Context {
	return &Context{
		Writer: w,
		Req:    req,
		Path:   req.URL.Path,
		Method: req.Method,
		index:  -1,
	}
}

func (c *Context) Next() {
	c.index++
	s := len(c.handlers)
	for ; c.index < s; c.index++ {
		c.handlers[c.index](c)
	}
}

func (c *Context) PostForm(key string) string {
	return c.Req.FormValue(key)
}

func (c *Context) Query(key string) string {
	return c.Req.URL.Query().Get(key)
}

func (c *Context) Status(status int) {
	c.StatusCode = status
	c.Writer.WriteHeader(status)
}

func (c *Context) SetHeader(key string, value string) {
	c.Writer.Header().Set(key, value)
}

//String HTTP JSON Data 是对各种 输出信息 输出 特定格式 的方法
//注意这里使用http标准库中的方法的顺序（虽然我们封装过了，后面使用可以不太注意了，但写框架的时候一定是要注意的）：
//先调用SetHeader(里面封装了Header.Set()),
//接着调用Status(里面封装了WriteHeader())，
//最后调用Write()
func (c *Context) String(status int, format string, values ...interface{}) {
	c.SetHeader("Content-Type", "text/plain")
	c.Status(status)
	c.Writer.Write([]byte(fmt.Sprintf(format, values...)))
}

func (c *Context) JSON(status int, obj interface{}) {
	c.SetHeader("Content-Type", "application/json")
	c.Status(status)
	encoder := json.NewEncoder(c.Writer)
	if err := encoder.Encode(obj); err != nil {
		//http.Error(c.Writer, err.Error(), 500)
		panic(err)
	}
	return
}

func (c *Context) Data(status int, data []byte) {
	c.Status(status)
	c.Writer.Write(data)
}

func (c *Context) HTML(status int, name string, data interface{}) {
	c.SetHeader("Content-Type", "text/html")
	c.Status(status)
	//根据模板文件名 选择模板进行渲染
	if err := c.engine.htmlTemplates.ExecuteTemplate(c.Writer, name, data); err != nil {
		c.Fail(500, err.Error())
	}
}

//Param 获取到在请求中解析到的参数
func (c *Context) Param(key string) string {
	value, _ := c.Params[key]
	return value
}

func (c *Context) Fail(code int, err string) {
	//if c == nil {
	//	c.index = len(c.handlers)
	//	c.JSON(code, H{"message": err})
	//}
	c.index = len(c.handlers)
	c.JSON(code, H{"message": err})
}
