package tuanWeb

import (
	"fmt"
	"log"
	"net/http"
	"runtime"
	"strings"
)

//获取触发panic时的堆栈信息
func trace(message string) string {
	var pcs [32]uintptr
	//runtime.Callers是用来返回调用栈的程序计数器
	//第0层是Callers自身，第1层是trace，第2层是defer func
	//(trace中使用了Callers,Recovery的defer func中使用了trace)
	//为了日志简洁，直接跳过了前3个Callers
	n := runtime.Callers(3, pcs[:]) // skip first 3 caller

	var str strings.Builder
	str.WriteString(message + "\nTraceback:")
	for _, pc := range pcs[:n] {
		//FuncForPC 获取对应的函数
		fn := runtime.FuncForPC(pc)
		//FileLine 获取调用该函数的文件名和行号
		file, line := fn.FileLine(pc)
		str.WriteString(fmt.Sprintf("\n\t%s:%d", file, line))
	}
	return str.String()
}

//Recovery 错误处理函数
func Recovery() HandlerFunc {
	return func(c *Context) {
		defer func() {
			if err := recover(); err != nil {
				message := fmt.Sprintf("%s", err)
				log.Printf("%s\n\n", trace(message))
				c.Fail(http.StatusInternalServerError, "Internal Server Error")
			}
		}()
		c.Next()
	}
}
